# AWX PAS CCP Integration

## Requirements

- AWX instance that can access:
	- a Password Vault Web Access (PVWA) over tcp/443
	- a Central Credential Provider (CCP) over tcp/443
- An AWX host named localhost
- A CyberArk CCP application ID (eg: AWX).
	- This account can be configured to authenticate via IP whitelisting and/or mutal TLS using a client certificate
	- This account should have necessary permissions to retreive the account decribed below in a CyberArk safe (eg: AWX_Onboard_Safe). 
- A CyberArk EPV account (eg: AWX_Onboard) used to communicate with PVWA API and store newly created privileged accounts.
	- This account must have necessary permissions to add accounts in target safes (eg: Unix_SSH_Keys safe)
	- This account must be stored securely in a safe as described above (eg: AWX_Onboard_Safe) 

## Step 1: CCP / AWX Integration

1. Log into AWX as an Administrator.
2. Go to Resources > Credentials and create a new credential using type "CyberArk AIM Central Credential Provider Lookup".
	- Set the CCP URL, AppID and Client Certificate if used.
	- Test lookup mechanism using an object query like so: `safe=AWX_Onboarded_Accounts;folder=Root;object=my_platform.my_domain-AWX_Onboard`
NB: The object is the name of the AWX_Onboard account filename stored in CyberArk Vault. Ask your CyberArk administrator for it or find it using the PVWA if you have access to the account.
3. Go to Administration > Credential Types and create a new credential type (eg: EPV Account) with the following parameters:
- Input configuration (yaml)
```yaml
fields:
  - id: epv_user
    type: string
    label: EPV Username
  - id: epv_password
    type: string
    label: EPV Password
    secret: true
required:
  - epv_user
  - epv_password
```
- Injector configuration (yaml)
```yaml
extra_vars:
  epv_user: '{{ epv_user }}'
  epv_password: '{{ epv_password }}'
```
4. Go to Resources > Credentials and create a new credential using type "EPV Account" (or the name you choose in step 3)
	- Fill in EPV Username with the name of your CyberArk EPV account (eg: AWX_Onboard)
	- Fill in EPV Password using the External Secret Management System (key symbol on the right) and choosing the CCP Lookup (configured at step 2).
		- The Object Query must fetch the EPV account:  `safe=AWX_Onboarded_Accounts;folder=Root;object=my_platform.my_domain-AWX_Onboard`
		- Query format must be `Exact`
		- Reason is optional (unless configured otherwise in CyberArk)

## Step 2: Account onboarding

You can now use the newly created EPV Account credential to onboard accounts from AWX.
Try use it with the example playbook:
1. In `main.yml` change platformId and safeName according to your CyberArk Platform and Safe configuration.
2. Configure a project that fetch main.yml and requirements.yml (which imports cyberark.modules from Ansible Galaxy)
3. Configure a template that runs main.yml and use your EPV Account.
	* You may add the PVWA url in your template variables: `pvwa_url: <PVWA url>`
4. Run the template. A new account called test_username has been stored in Cyberark.
